= Bootstrap Antora Task

== Objective

.The task `bootstrapAntora` aims to:
* create the initial directories and files to get started with Antora

== How It Works

The task `bootstrapAntora` creates the Antora directories
with modules and family directories
and writes index page, navigation and component descriptor.

NOTE: The task will *NOT* overwrite existing files.

== Setup

[source,gradle,subs="+attributes"]
.build.gradle
--
plugins {
    id 'com.gitlab.mvik.local-antora' version '{project-version}' apply false
}
--

== Run Bootstrap

[source.bash]
--
./gradlew bootstrapAntora --modules=ROOT
--

=== Parameters

[width="80%",cols="3m,4d,2m"]
|===
| Parameter | Description | Default

| target-directory | The root directory of the Antora component,
                     the directory where `antora.yml` is placed | docs
| name             | The name of the component                  | <project-name>
| title            | The title                                  |  <Capitalized Project Name>
| version          | The component version                      | master
| prerelease       | If this is a prerelease                    | false
| modules          | Comma-separated list of modules            | ROOT
|===

include::partial$more-examples.adoc[leveloffset=+1]
