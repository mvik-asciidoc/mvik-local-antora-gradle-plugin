package mvik.gradle.plugin.kroki

import org.gradle.api.GradleException
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class KrokiFileBuilderTest extends Specification {

    @Subject KrokiFileBuilder writer = new KrokiFileBuilder()

    def 'build docker compose yaml for kroki services'() {
        when:
            def result = writer.createYaml('0.11.0', 8000)
        then:
            println result
            result.services.core != null
            result.services.bpmn != null
    }

    @Unroll
    def 'resolve verified kroki services when #description'() {
        when:
            def verified = KrokiFileBuilder.resolveKrokiServices(input)
        then:
            verified == expected
        where:
            description                                     | input                         | expected
            'all'                                           | ['all']                       | KrokiFileBuilder.KROKI_SERVICES_ALL
            'ALL'                                           | ['ALL']                       | KrokiFileBuilder.KROKI_SERVICES_ALL
            'only core'                                     | ['core']                      | input
            'none'                                          | ['none']                      | []
            'none and other should be empty'                | ['none', 'core']              | []
            'NONE and other should be empty'                | ['NONE', 'core']              | []
            'core must be added when missing'               | ['mermaid']                   | ['core'] + input
            'core must be added when missing and immutable' | Arrays.asList('mermaid')      | ['core'] + input
            'core and more'                                 | ['core', 'blockdiag', 'bpmn'] | input
            'wrong caps'                                    | ['CORE', 'BlockDiag', 'BpMn'] | input*.toLowerCase()
    }

    @Unroll
    def 'reject invalid services #services'() {
        when:
            KrokiFileBuilder.resolveKrokiServices(services)
        then:
            def e = thrown(GradleException)
            !e.message.trim().isEmpty()
        where:
            services << [
                ['kork'],
                ['all', 'unknown'],
                ['none', 'unknown'],
            ]
    }

}
