package mvik.gradle.plugin.kroki

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.BuildTask
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class LocalKrokiPluginTest extends Specification {

    @Rule TemporaryFolder tf = new TemporaryFolder()

    def 'apply plugin and write kroki file'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-kroki'
                }
                
                localKroki {
                    composeFile.set(layout.buildDirectory.file('kroki/docker-compose.yml'))
                }
                ''',
                'writeKroki'
            )
        then:
            BuildTask writeKrokiTask = result.tasks.find {
                it.path ==~ /:writeKroki.*/
            }
            writeKrokiTask.outcome == TaskOutcome.SUCCESS
        and:
            File krokiFile = new File(tf.root, 'build/kroki/docker-compose.yml')
            krokiFile.isFile()
    }

    private BuildResult buildAndRunTask(String build, String... arguments) {
        File buildFile = tf.newFile('build.gradle')
        buildFile.text = build
        return GradleRunner.create()
            .withProjectDir(tf.root)
            .withArguments(arguments)
            .withPluginClasspath()

            .build()
    }

}
