package mvik.gradle.plugin.yaml

import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import spock.lang.Specification
import spock.lang.Subject

class SnakeYamlTest extends Specification {

    @Subject Yaml yaml = new Yaml(
        new DumperOptions(
            prettyFlow: true,
            defaultFlowStyle: DumperOptions.FlowStyle.BLOCK
        )
    )

    def 'write yaml'() {
        given:
            Map<String, Object> playbook = [
                site    : [
                    title     : 'Local Site',
                    start_page: ''
                ],
                content : [
                    sources: [
                        url       : '.',
                        start_path: 'docs',
                        branches  : 'HEAD'
                    ]
                ],
                asciidoc: [
                    extensions: [
                        'asciidoctor-kroki',
                        '@djencks/asciidoctor-mathjax'
                    ],
                    attributes: [
                        toc                  : '~',
                        'kroki-server-url'   : 'http://localhost:8000',
                        'kroki-fetch-diagram': true
                    ]
                ],
                runtime : [
                    cache_dir: './.cache/antora',
                    fetch    : true
                ],
                ui      : [
                    bundle: [
                        url     : 'https://path.to/custom-ui-bundle.zip',
                        snapshot: true
                    ]
                ],
                output  : [
                    destinations: [
                        provider: 'fs',
                        path    : 'build/antora-site'
                    ]
                ]

            ]
        when:
            def writer = new StringWriter()
            yaml.dump(playbook, writer)
        then:
            println writer
    }


}
