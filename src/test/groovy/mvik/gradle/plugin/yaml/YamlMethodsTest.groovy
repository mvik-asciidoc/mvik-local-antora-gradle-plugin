package mvik.gradle.plugin.yaml

import org.yaml.snakeyaml.Yaml
import spock.lang.Specification

class YamlMethodsTest extends Specification {

    Yaml yaml = YamlFactory.yaml()


    def 'remove apostrophes around ~'() {
        given:
            String input = yaml.dump(
                YamlMap.start()
                    .with(
                        "attributes",
                        YamlMap.start()
                            .with("toc", "~")
                    )
            )
        when:
            String result = YamlMethods.fixAttributeUnset(input)

        then:
            result.contains('toc: ~')
    }
}
