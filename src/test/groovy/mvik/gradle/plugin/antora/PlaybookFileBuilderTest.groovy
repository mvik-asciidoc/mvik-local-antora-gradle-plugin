package mvik.gradle.plugin.antora

import mvik.gradle.plugin.yaml.YamlFactory
import mvik.gradle.plugin.yaml.YamlMap
import org.yaml.snakeyaml.Yaml
import spock.lang.Specification
import spock.lang.Unroll

class PlaybookFileBuilderTest extends Specification {

    private Yaml yaml = YamlFactory.yaml()

    def 'build antora playbook yaml'() {
        when:
            def yamlMap = createYamlMapWithTestDefaults()
        then:
            yamlMap
        when:
            def result = yaml.dump(yamlMap)
        then:
            result.contains 'branches: HEAD'
            result.contains 'kroki-server-url: http://localhost:8000'
            result.contains 'kroki-fetch-diagram: true'
            result.contains 'attr: value'
    }

    @Unroll
    def 'handle kroki server url correctly when #description'() {
        when:
            def result = yaml.dump(
                createYamlMapWithTestDefaults([
                    krokiFetchDiagram : false,
                    krokiServerUrl    : krokiServerUrl,
                    asciidocAttributes: asciidocAttributes
                ])
            )
        then:
            println result
            result.count('kroki-server-url:') == attributeCount

        where:
            description                            | krokiServerUrl          | asciidocAttributes         | attributeCount
            'not set'                              | null                    | [:]                        | 0
            'empty'                                | ''                      | [:]                        | 0
            'default'                              | 'https://kroki.io'      | [:]                        | 1
            'custom'                               | 'http://localhost:8000' | [:]                        | 1
            'removed using override in attributes' | 'http://localhost:8000' | ['kroki-server-url': null] | 0
    }

    private static YamlMap createYamlMapWithTestDefaults(Map<String, Object> overrides = [:]) {
        def defaults = [
            title             : 'Local Site',
            startPage         : 'my-site::index.adoc',
            repositoryRoot    : '/path/to/git/root',
            startPaths        : ['docs'],
            tags              : null,
            branches          : ['HEAD'],
            asciidocExtensions: ['asciidoctor-kroki'],
            krokiServerUrl    : 'http://localhost:8000',
            krokiFetchDiagram : true,
            asciidocAttributes: [attr: 'value', 'some-other-attribute': 'some other value'],
            uiBundleUrl       : './custom-ui-bundle.zip',
            supplementalFiles : null,
            siteDir           : 'build/antora-site'
        ]
        def merged = defaults + overrides
        return PlaybookFileBuilder.createYamlMap(
            merged.title as String,
            merged.startPage as String,
            merged.repositoryRoot as String,
            merged.startPaths as List<String>,
            merged.branches as List<String>,
            merged.tags as List<String>,
            merged.asciidocExtensions as List<String>,
            merged.krokiServerUrl as String,
            merged.krokiFetchDiagram as boolean,
            merged.asciidocAttributes as Map<String, Object>,
            merged.uiBundleUrl as String,
            merged.supplementalFiles as String,
            merged.siteDir as String
        )
    }
}
