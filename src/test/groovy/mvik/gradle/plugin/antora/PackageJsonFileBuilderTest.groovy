package mvik.gradle.plugin.antora

import groovy.json.JsonSlurper
import spock.lang.Specification
import spock.lang.Unroll

class PackageJsonFileBuilderTest extends Specification {

    def 'build package.json'() {
        when:
            String json = PackageJsonFileBuilder.createPackageJson(
                '^2.3.4',
                '2.2.1',
                [:]
            )
            println json
        then:
            def parsed = new JsonSlurper().parseText(json) as Map
            parsed.private == true
            parsed.scripts instanceof Map
            parsed.devDependencies instanceof Map
    }

    @Unroll
    def 'only add npm dependencies'() {
        when:
            String json = PackageJsonFileBuilder.createPackageJson(
                '2,3,4',
                '2.2.1',
                extensions
            )
        then:
            def parsed = new JsonSlurper().parseText(json) as Map
            def deps = parsed.devDependencies as Map<String, String>
            deps.containsKey('a') == added
        where:
            extensions   | added
            [a: '1.0.1'] | true
            [a: '^1']    | true
            [a: null]    | false
            [a: '']      | false
    }
}
