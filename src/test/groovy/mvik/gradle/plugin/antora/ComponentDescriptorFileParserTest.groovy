package mvik.gradle.plugin.antora

import spock.lang.Specification

class ComponentDescriptorFileParserTest extends Specification {

    def 'no start page'() {
        when:
            def startPage = ComponentDescriptorFileParser.extractStartPage(
                // language=yaml
                '''
                name: demo-docs
                '''.stripIndent()
            )
        then:
            startPage == null
    }

    def 'start page from ROOT module and index.adoc'() {
        when:
            def startPage = ComponentDescriptorFileParser.extractStartPage(
                // language=yaml
                '''
                name: demo-docs
                start_page: ROOT:index.adoc
                '''.stripIndent()
            )
        then:
            startPage == 'demo-docs:ROOT:index.adoc'
    }

    def 'start page from some other module and adoc file'() {
        when:
            def startPage = ComponentDescriptorFileParser.extractStartPage(
                // language=yaml
                '''
                name: my-component
                start_page: module-b:overview.adoc
                '''.stripIndent()
            )
        then:
            startPage == 'my-component:module-b:overview.adoc'
    }
}
