package mvik.gradle.plugin.antora

import spock.lang.Specification
import spock.lang.Unroll

import java.nio.file.Path
import java.nio.file.Paths

class WriteAntoraPlaybookFileTaskTest extends Specification {

    @SuppressWarnings('GroovyAccessibility')
    @Unroll
    def 'strip repository root from start paths'() {
        when:
            Path relative = WriteAntoraPlaybookFileTask.relativizePath(
                source,
                target
            )
        then:
            relative as String == expected
        where:
            source                    | target                                   | expected
            Paths.get('/dev/my-proj') | Paths.get('/dev/my-proj/the/start/path') | 'the/start/path'
            Paths.get('/dev/my-proj') | Paths.get('the/start/path')              | 'the/start/path'
            Paths.get('/dev/my-proj') | null                                     | null
            Paths.get('/a/b/c')       | Paths.get('/1/2/3')                      | '../../../1/2/3'
    }

}
