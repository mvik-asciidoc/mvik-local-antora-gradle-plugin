package mvik.gradle.plugin.antora


import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.BuildTask
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.gradle.testkit.runner.UnexpectedBuildFailure
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import org.yaml.snakeyaml.Yaml
import spock.lang.IgnoreIf
import spock.lang.Specification
import spock.lang.Unroll

class LocalAntoraPluginTest extends Specification {

    @Rule TemporaryFolder tf = new TemporaryFolder()

    private final Yaml yaml = new Yaml()

    def 'apply plugin'() {
        given:
            Project project = ProjectBuilder.builder().build()
        when:
            project.plugins.apply('com.gitlab.mvik.local-antora')
        then:
            project.plugins.size() > 0
    }

    @IgnoreIf({ System.getenv('CI') != null })
    def 'apply plugin and write playbook file'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-kroki'
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    asciidocExtension 'asciidoctor-pipedream', '^0.9.0'
                    
                    kroki {
                        serverUrl.set localKroki.url
                        fetchDiagram.set true
                    }

                    playbook {
                        startPage.set 'my-component::index.adoc'
                        asciidocAttributes.set(
                            'author': 'Mikael Vik',
                            'kebab-case': true 
                        )
                        uiBundleUrl.set '/path/override/custom-ui-bundle.zip'
                    }
                }
                ''',
                'writeAntoraPlaybook'
            )
        then:
            BuildTask writePlaybookTask = findTask(result, 'writeAntora')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()
        and: 'extensions are added'
            with(playbookFile.text) {
                println it
                it.contains('asciidoctor-kroki')
                it.contains('@djencks/asciidoctor-mathjax')
                it.contains('asciidoctor-pipedream')
            }
    }

    def 'apply plugin and write package.json file'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    antoraVersion '^2.3.4'
                    asciidocExtension 'asciidoctor-pipedream', '^0.9.0'
                }
                ''',
                'writePackageJson', '--stacktrace')

        then:
            BuildTask writePackageJsonTask = findTask(result, 'writePackageJson')
            writePackageJsonTask.outcome == TaskOutcome.SUCCESS
        and:
            def packageJsonFile = new File(tf.root, 'package.json')
            packageJsonFile.isFile()
            with(packageJsonFile.text) {
                println it
                it.contains('"asciidoctor-kroki"')
                it.contains('"@djencks/asciidoctor-mathjax"')
                it.contains('"asciidoctor-pipedream" : "^0.9.0"')
            }
    }

    @IgnoreIf({ System.getenv('CI') != null })
    def 'apply plugin and handle no asciidoc extensions'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    asciidocExtensions.set([:])
                }
                ''',
                'writeAntora', 'writePackageJson', '--stacktrace')

        then:
            BuildTask writePlaybookTask = findTask(result, 'writeAntora')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()
            println playbookFile.text
        then:
            BuildTask writePackageJsonTask = result.tasks.find {
                it.path ==~ /:writePackageJson.*/
            }
            writePackageJsonTask.outcome == TaskOutcome.SUCCESS
        and:
            def packageJsonFile = new File(tf.root, 'package.json')
            packageJsonFile.isFile()
            println packageJsonFile.text
    }

    @IgnoreIf({ System.getenv('CI') != null })
    def 'apply plugin should use kroki.io as default'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                ''',
                'writeAntoraPlaybook', '--stacktrace')
        then:
            BuildTask writePlaybookTask = findTask(result, 'writeAntoraPlaybook')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()

            Map playbook = yaml.load(playbookFile.text) as Map<String, Map>
            Map attributes = playbook.asciidoc.attributes as Map
            attributes.'kroki-server-url' == 'https://kroki.io'
    }

    @Unroll
    @IgnoreIf({ System.getenv('CI') != null })
    def 'apply plugin along with kroki plugin should configure local kroki as default'() {
        when:
            BuildResult result = buildAndRunTask(
                """
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                    id 'com.gitlab.mvik.local-kroki'
                }
                
                localKroki {
                    port.set ${krokiPort}
                }
                """,
                'writeAntoraPlaybook', '--stacktrace')
        then:
            println result.output
            BuildTask writePlaybookTask = findTask(result, 'writeAntoraPlaybook')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()

            Map playbook = yaml.load(playbookFile.text) as Map<String, Map>
            Map attributes = playbook.asciidoc.attributes as Map
            attributes.'kroki-server-url' == "http://localhost:${krokiPort}"
        where:
            krokiPort << [8000, 8080, 9000]
    }

    @IgnoreIf({ System.getenv('CI') != null })
    def "apply plugin without startPaths should default to 'docs'"() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                """
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    playbook {
                        repositoryRoot.set '/private${tf.root.absolutePath}'
                    }
                }
                """,
                'writeAntoraPlaybook', '--stacktrace')
        then:
            BuildTask writePlaybookTask = findTask(result, 'writeAntoraPlaybook')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()

            Map playbook = yaml.load(playbookFile.text) as Map<String, Map>
            List<Map> sources = playbook.content.sources as List<Map>
            List<String> startPaths = sources[0].start_paths as List<String>
            startPaths == ['docs']
    }

    def 'supplemental_files should be a directory'() {
        given:
            tf.newFolder('docs', 'supplemental_files')
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    playbook {
                        repositoryRoot.set layout.projectDirectory.asFile.absolutePath
                        uiSupplementalFiles.set layout.projectDirectory.dir('docs/supplemental_files') 
                    }
                }
                ''',
                'writeAntoraPlaybook', '--stacktrace')
        then:
            BuildTask writePlaybookTask = findTask(result, 'writeAntoraPlaybook')
            writePlaybookTask.outcome == TaskOutcome.SUCCESS
        and:
            def playbookFile = new File(tf.root, 'build/antora/playbook.yml')
            playbookFile.isFile()

            Map playbook = yaml.load(playbookFile.text) as Map<String, Map>
            playbook.ui.supplemental_files ==~ '.*/docs/supplemental_files'
    }

    // should have used GradleRunner::buildAndFail()
    def 'fail if supplemental_files not a directory'() {
        when:
            buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'com.gitlab.mvik.local-antora'
                }
                
                localAntora {
                    playbook {
                        repositoryRoot.set layout.projectDirectory.asFile.absolutePath
                        uiSupplementalFiles.set layout.projectDirectory.dir('docs/supplemental_files') 
                    }
                }
                ''',
                'writeAntoraPlaybook', '--stacktrace')
        then:
            thrown(UnexpectedBuildFailure)
    }

    private BuildResult buildAndRunTask(String build, String... arguments) {
        File buildFile = tf.newFile('build.gradle')
        buildFile.text = build
        return GradleRunner.create()
            .withProjectDir(tf.root)
            .withArguments(arguments)
            .withPluginClasspath()
            .build()
    }

    private static BuildTask findTask(BuildResult result, String partialTaskName) {
        return result.tasks.find {
            it.path ==~ "^:${partialTaskName}.*"
        }
    }

}
