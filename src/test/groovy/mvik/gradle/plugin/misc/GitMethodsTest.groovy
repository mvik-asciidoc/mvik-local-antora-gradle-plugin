package mvik.gradle.plugin.misc

import spock.lang.IgnoreIf
import spock.lang.Specification

class GitMethodsTest extends Specification {

    @IgnoreIf({ System.getenv('CI') != null })
    def 'locate git repository'() {
        when:
            String repositoryRoot = GitMethods.locateGitRepositoryRootDirectory()
        then:
            repositoryRoot != null
        when:
            File gitDir = new File(repositoryRoot, '.git')
        then:
            gitDir.isDirectory()
    }
}
