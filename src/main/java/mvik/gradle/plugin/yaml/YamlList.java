package mvik.gradle.plugin.yaml;

import java.util.ArrayList;
import java.util.Collections;

public class YamlList extends ArrayList<Object> {

    public static YamlList of(Object... values) {
        YamlList list = new YamlList();
        Collections.addAll(list, values);
        return list;
    }
}
