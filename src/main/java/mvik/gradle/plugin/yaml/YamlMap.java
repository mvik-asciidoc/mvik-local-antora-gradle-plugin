package mvik.gradle.plugin.yaml;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Objects;

public class YamlMap extends LinkedHashMap<String, Object> {

    public static YamlMap start() {
        return new YamlMap();
    }

    public YamlMap with(String key, Object value) {
        put(key, value);
        return this;
    }

    public YamlMap withoutNullValues() {
        this.values().removeIf(Objects::isNull);
        return this;
    }

    public YamlMap withIfNotEmpty(String key, Object value) {
        if (value instanceof Collection) {
            return withIfNotEmpty(key, (Collection<?>) value);
        }
        if (value != null && !value.toString().trim().isEmpty()) {
            put(key, value);
        }
        return this;
    }

    public YamlMap withIfNotEmpty(String key, Collection<?> values) {
        if (values != null && !values.isEmpty()) {
            put(key, values);
        }
        return this;
    }

    public YamlMap withIfNotNull(String key, Object value) {
        if (key != null && value != null) {
            put(key, value);
        }
        return this;
    }

    public YamlMap withIfValue(String key, Object value) {
        if (key != null && value != null) {
            if (value instanceof String && ((String) value).trim().isEmpty()) {
                return this;
            }
            put(key, value);
        }
        return this;
    }
}
