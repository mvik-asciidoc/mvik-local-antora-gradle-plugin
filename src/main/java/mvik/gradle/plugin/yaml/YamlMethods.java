package mvik.gradle.plugin.yaml;

public final class YamlMethods {

    public static String dumpForAntora(Object data) {
        return fixAttributeUnset(
            YamlFactory.yaml().dump(data)
        );
    }

    static String fixAttributeUnset(String yaml) {
        return yaml.replaceAll(
            ": '~'",
            ": ~"
        );
    }
}
