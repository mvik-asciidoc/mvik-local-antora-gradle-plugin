package mvik.gradle.plugin.kroki;

import mvik.gradle.plugin.yaml.YamlFactory;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;
import org.yaml.snakeyaml.Yaml;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings({"UnstableApiUsage", "ResultOfMethodCallIgnored"})
public class WriteKrokiDockerComposeFileTask extends DefaultTask {

    @Input
    private final Property<String> krokiVersion;

    @Input
    private final Property<Integer> exposedPort;

    @Optional
    @OutputFile
    private final RegularFileProperty composeFile;

    @Inject
    public WriteKrokiDockerComposeFileTask(ObjectFactory objects) {
        this.krokiVersion = objects.property(String.class);
        this.exposedPort = objects.property(Integer.class);
        this.composeFile = objects.fileProperty();
    }

    public Property<String> getKrokiVersion() {
        return krokiVersion;
    }

    public Property<Integer> getExposedPort() {
        return exposedPort;
    }

    public RegularFileProperty getComposeFile() {
        return composeFile;
    }

    @TaskAction
    void writeFile() throws IOException {

        File file = getComposeFile().getAsFile().get();

        if (!file.isFile()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }

        FileWriter writer = new FileWriter(file);
        Yaml yaml = YamlFactory.yaml();

        yaml.dump(
            KrokiFileBuilder.createYaml(krokiVersion.get(), exposedPort.get()),
            writer
        );
        writer.close();

        getLogger().lifecycle("Kroki docker-compose file has been written to: " + file.getAbsolutePath());
    }
}
