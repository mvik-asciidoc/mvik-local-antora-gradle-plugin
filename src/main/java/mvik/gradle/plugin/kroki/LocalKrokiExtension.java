package mvik.gradle.plugin.kroki;

import org.gradle.api.file.ProjectLayout;
import org.gradle.api.file.RegularFile;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unused"})
public abstract class LocalKrokiExtension {

    public abstract Property<String> getVersion();

    public abstract Property<Integer> getPort();

    public abstract ListProperty<String> getServices();

    public abstract RegularFileProperty getComposeFile();

    public abstract Property<Boolean> getKrokiOnly();

    @Inject
    public LocalKrokiExtension(ProjectLayout layout) {
        getVersion().convention("0.16.0");
        getPort().convention(8000);
        getServices().convention(Collections.singletonList("core"));
        getComposeFile().convention(layout.getBuildDirectory().file("kroki/docker-compose.yml"));
        getKrokiOnly().convention(true);
    }

    public Provider<String> getUrl() {
        return getPort().map(it -> "http://localhost:" + it);
    }

    public void version(String version) {
        getVersion().set(version);
    }

    public void port(int port) {
        getPort().set(port);
    }

    public void services(List<String> services) {
        getServices().set(services);
    }

    public void service(String service) {
        getServices().add(service);
    }

    public void composeFile(Provider<RegularFile> composeFile) {
        getComposeFile().set(composeFile);
    }

    public void krokiOnly(boolean krokiOnly) {
        getKrokiOnly().set(krokiOnly);
    }
}
