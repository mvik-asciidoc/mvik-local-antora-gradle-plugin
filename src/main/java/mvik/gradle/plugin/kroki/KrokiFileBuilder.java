package mvik.gradle.plugin.kroki;

import mvik.gradle.plugin.yaml.YamlList;
import mvik.gradle.plugin.yaml.YamlMap;
import org.gradle.api.GradleException;
import org.gradle.api.NonNullApi;
import org.gradle.internal.impldep.org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

final class KrokiFileBuilder {

    private static final String KROKI_SERVICE_CORE = "core";
    private static final String KROKI_SERVICE_BLOCKDIAG = "blockdiag";
    private static final String KROKI_SERVICE_MERMAID = "mermaid";
    private static final String KROKI_SERVICE_BPMN = "bpmn";
    private static final String KROKI_SERVICE_EXCALIDRAW = "excalidraw";

    static final List<String> KROKI_SERVICES_ALL = Arrays.asList(
        KROKI_SERVICE_CORE,
        KROKI_SERVICE_BLOCKDIAG,
        KROKI_SERVICE_MERMAID,
        KROKI_SERVICE_BPMN,
        KROKI_SERVICE_EXCALIDRAW
    );

    static YamlMap createYaml(String krokiVersion, int exposedPort) {

        return YamlMap.start()
            .with("version", "3")
            .with("services",
                YamlMap.start()
                    .with(KROKI_SERVICE_CORE, YamlMap.start()
                        .with("image", "yuzutech/kroki:" + krokiVersion)
                        .with("environment", YamlList.of(
                            "KROKI_BLOCKDIAG_HOST=blockdiag",
                            "KROKI_MERMAID_HOST=mermaid",
                            "KROKI_BPMN_HOST=bpmn",
                            "KROKI_EXCALIDRAW_HOST=excalidraw")
                        )
                        .with("ports", YamlList.of(exposedPort + ":8000"))
                    )
                    .with(KROKI_SERVICE_BLOCKDIAG, createService(krokiVersion, "blockdiag", "8001"))
                    .with(KROKI_SERVICE_MERMAID, createService(krokiVersion, "mermaid", "8002"))
                    .with(KROKI_SERVICE_BPMN, createService(krokiVersion, "bpmn", "8003"))
                    .with(KROKI_SERVICE_EXCALIDRAW, createService(krokiVersion, "excalidraw", "8004"))

            );
    }

    private static YamlMap createService(String krokiVersion, String service, String port) {
        return YamlMap.start()
            .with("image", "yuzutech/kroki-" + service + ":" + krokiVersion)
            .with("expose", YamlList.of(port));
    }

    static List<String> resolveKrokiServices( List<String> services) {
        services.forEach(s -> verifyKnownService(s));

        if (services.stream().anyMatch(s -> s.equalsIgnoreCase("all"))) {
            return KROKI_SERVICES_ALL;
        }
        if (services.stream().anyMatch(s -> s.equalsIgnoreCase("none"))) {
            return Collections.emptyList();
        }

        List<String> resolved = services.stream()
            .map(s -> s.toLowerCase())
            .peek(s -> verifyKnownService(s))
            .collect(Collectors.toList());

        if (!resolved.isEmpty() && !resolved.contains(KROKI_SERVICE_CORE)) {
            resolved.add(0, KROKI_SERVICE_CORE);
        }
        return resolved;
    }

    private static void verifyKnownService(String s) {
        if (s.equalsIgnoreCase("all") || s.equalsIgnoreCase("none")) {
            return;
        }
        if (!KROKI_SERVICES_ALL.contains(s.toLowerCase())) {
            throw new GradleException("Unknown Kroki service [" + s + "]");
        }
    }
}
