package mvik.gradle.plugin.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public final class GitMethods {

    public static String locateGitRepositoryRootDirectory() throws IOException {
        Process process = Runtime.getRuntime().exec(
            "git rev-parse --show-toplevel"
        );
        return new BufferedReader(new InputStreamReader(process.getInputStream()))
            .lines()
            .collect(Collectors.joining("\n"));
    }
}
