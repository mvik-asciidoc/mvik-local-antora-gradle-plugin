package mvik.gradle.plugin.misc;

import org.gradle.api.GradleException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public final class FileWriterMethods {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static File writeToFile(File target, String result) {
        if (!target.isFile()) {
            target.getParentFile().mkdirs();
        }

        try (FileWriter writer = new FileWriter(target)) {
            writer.write(result);
            return target;
        } catch (IOException e) {
            throw new GradleException("Failed to write to file", e);
        }
    }

}
