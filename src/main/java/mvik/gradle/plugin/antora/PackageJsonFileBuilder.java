package mvik.gradle.plugin.antora;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.jr.ob.JSONComposer;
import com.fasterxml.jackson.jr.ob.comp.ObjectComposer;

import java.io.IOException;
import java.util.Map;

class PackageJsonFileBuilder {

    private static final JSON JSON_PRETTY = JSON.std.with(JSON.Feature.PRETTY_PRINT_OUTPUT);

    static String createPackageJson(String antoraVersion, String asciidoctorCoreVersion, Map<String, String> asciidocExtensions) throws IOException {

        // @formatter:off
        ObjectComposer<ObjectComposer<JSONComposer<String>>> devDeps = JSON_PRETTY
            .composeString()
            .startObject()
                .put("private", true)
                .startObjectField("scripts")
                    .put("clean", "rm -rf node_modules .cache/ package-lock.json")
                    .put("antora", "node_modules/.bin/antora --stacktrace --fetch")
                .end()
                .startObjectField("devDependencies")
                    .put("@antora/cli", antoraVersion)
                    .put("@antora/site-generator-default", antoraVersion)
                    .put("@asciidoctor/core", asciidoctorCoreVersion);
        // @formatter:on

        if (asciidocExtensions != null) {
            for (Map.Entry<String, String> e : asciidocExtensions.entrySet()) {
                if (e.getValue() != null && !e.getValue().trim().isEmpty()) {
                    devDeps.put(e.getKey(), e.getValue());
                }
            }
        }
        // @formatter:off
        return devDeps
                .end()
            .end()
            .finish();
        // @formatter:on
    }
}
