package mvik.gradle.plugin.antora;

import org.gradle.api.provider.Property;

@SuppressWarnings("unused")
public abstract class Kroki {

    public abstract Property<String> getServerUrl();

    public abstract Property<Boolean> getFetchDiagram();

    public void serverUrl(String serverUrl) {
        getServerUrl().set(serverUrl);
    }

    public void fetchDiagram(boolean fetchDiagram) {
        getFetchDiagram().set(fetchDiagram);
    }
}
