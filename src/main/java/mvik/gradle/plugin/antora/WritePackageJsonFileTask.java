package mvik.gradle.plugin.antora;

import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings({"ResultOfMethodCallIgnored"})
public abstract class WritePackageJsonFileTask extends DefaultTask {

    private static final String DEFAULT_ANTORA_VERSION = "^3.1.2";
    private static final String DEFAULT_ASCIIDOCTOR_CORE_VERSION = "^2.2.6";

    @Optional
    @Input
    public abstract Property<String> getAntoraVersion();

    @Optional
    @Input
    public abstract Property<String> getAsciidoctorCoreVersion();

    @Optional
    @Input
    public abstract MapProperty<String, String> getAsciidocExtensions();

    @OutputFile
    public abstract RegularFileProperty getPackageJsonFile();

    @TaskAction
    void writeFile() throws IOException {

        File file = getPackageJsonFile().getAsFile().get();

        if (!file.isFile()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }

        FileWriter writer = new FileWriter(file);
        writer.write(
            PackageJsonFileBuilder.createPackageJson(
                getAntoraVersion().getOrElse(DEFAULT_ANTORA_VERSION),
                getAsciidoctorCoreVersion().getOrElse(DEFAULT_ASCIIDOCTOR_CORE_VERSION),
                getAsciidocExtensions().getOrNull()
            )
        );
        writer.close();

        getLogger().lifecycle("Antora package.json file has been written to: " + file.getAbsolutePath());
    }
}
