package mvik.gradle.plugin.antora;

import mvik.gradle.plugin.yaml.YamlFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

public class ComponentDescriptorFileParser {

    private static final Yaml YAML = YamlFactory.yaml();

    public static String extractStartPage(String componentDescriptor) {
        return extractStartPage(
            YAML.<Map<String, Object>>load(componentDescriptor)
        );
    }

    public static String extractStartPage(File componentDescriptor) {
        try {
            FileInputStream inputStream = new FileInputStream(componentDescriptor);
            return extractStartPage(
                YAML.<Map<String, Object>>load(inputStream)
            );
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    private static String extractStartPage(Map<String, Object> parsed) {

        Object name = parsed.get("name");
        Object startPage = parsed.get("start_page");

        if (hasValue(name) && hasValue(startPage)) {
            return name + ":" + startPage;
        } else {
            return null;
        }
    }

    private static boolean hasValue(Object o) {
        return o != null && !o.toString().trim().isEmpty();
    }
}
