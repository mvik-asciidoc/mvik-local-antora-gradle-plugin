package mvik.gradle.plugin.antora;

import org.gradle.api.file.Directory;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.RegularFile;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;

import java.util.List;
import java.util.Map;

@SuppressWarnings({"UnstableApiUsage", "unused"})
public abstract class Playbook {

    public abstract Property<Boolean> getRelativizePaths();

    public abstract Property<String> getTitle();

    public abstract Property<String> getStartPage();

    public abstract Property<String> getRepositoryRoot();

    public abstract ListProperty<String> getStartPaths();

    public abstract ListProperty<String> getBranches();

    public abstract ListProperty<String> getTags();

    public abstract Property<Boolean> getAddConvenienceAsciidocAttributes();

    public abstract MapProperty<String, Object> getAsciidocAttributes();

    public abstract Property<String> getUiBundleUrl();

    public abstract DirectoryProperty getUiSupplementalFiles();

    public abstract DirectoryProperty getOutputSiteDir();

    public void relativizePaths(boolean relativizePaths) {
        getRelativizePaths().set(relativizePaths);
    }

    public void title(String title) {
        getTitle().set(title);
    }

    public void startPage(String startPage) {
        getStartPage().set(startPage);
    }

    public void repositoryRoot(String repositoryRoot) {
        getRepositoryRoot().set(repositoryRoot);
    }

    public void repositoryRoot(RegularFile repositoryRoot) {
        getRepositoryRoot().set(repositoryRoot.getAsFile().getAbsolutePath());
    }

    public void startPaths(List<String> startPaths) {
        getStartPaths().set(startPaths);
    }

    public void startPath(String startPath) {
        getStartPaths().add(startPath);
    }

    public void startPath(RegularFile startPath) {
        getStartPaths().add(startPath.getAsFile().getAbsolutePath());
    }

    public void branches(List<String> branches) {
        getBranches().set(branches);
    }

    public void tags(List<String> tags) {
        getTags().set(tags);
    }

    public void addConvenienceAsciidocAttributes(boolean addConvenienceAsciidocAttributes) {
        getAddConvenienceAsciidocAttributes().set(addConvenienceAsciidocAttributes);
    }

    public void asciidocAttributes(Map<String, Object> asciidocAttributes) {
        getAsciidocAttributes().set(asciidocAttributes);
    }

    public void uiBundleUrl(String uiBundleUrl) {
        getUiBundleUrl().set(uiBundleUrl);
    }

    public void uiSupplementalFiles(Directory uiSupplementalFiles) {
        getUiSupplementalFiles().set(uiSupplementalFiles);
    }

    public void outputSiteDir(Provider<Directory> outputSiteDir) {
        getOutputSiteDir().set(outputSiteDir);
    }

    public void outputSiteDir(DirectoryProperty outputSiteDir) {
        getOutputSiteDir().set(outputSiteDir);
    }
}
