package mvik.gradle.plugin.antora;

import mvik.gradle.plugin.misc.FileWriterMethods;
import mvik.gradle.plugin.yaml.YamlMethods;
import mvik.gradle.plugin.yaml.YamlMap;
import org.gradle.api.DefaultTask;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.options.Option;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BootstrapAntoraTask extends DefaultTask {

    private static final String DEFAULT_ANTORA_ROOT_DIRECTORY = "docs";
    private static final List<String> ANTORA_FAMILY_DIRECTORIES = Arrays.asList(
        "attachments",
        "examples",
        "images",
        "pages",
        "partials"
    );

    @Input
    @Optional
    @Option(option = "target-directory", description = "Component directory (root)")
    public abstract Property<String> getTargetDirectory();

    @Input
    @Optional
    @Option(option = "name", description = "Component name")
    public abstract Property<String> getComponentName();

    @Input
    @Optional
    @Option(option = "title", description = "Component title")
    public abstract Property<String> getTitle();

    @Input
    @Optional
    @Option(option = "version", description = "Component version")
    public abstract Property<String> getVersion();

    @Input
    @Optional
    @Option(option = "prerelease", description = "Component prerelease identifier")
    public abstract Property<String> getPrerelease();

    @Input
    @Optional
    @Option(option = "modules", description = "Module directories")
    public abstract Property<String> getModulesString();

    private List<String> getModules() {
        return getModulesString()
            .map(it -> Arrays.asList(it.split(", *")))
            .getOrElse(Collections.singletonList("ROOT"));
    }

    @TaskAction
    void bootstrap() {
        getLogger().lifecycle("Bootstrap Antora directories and pages: ");

        File rootDir = mkdir(getTargetDirectory().getOrElse(DEFAULT_ANTORA_ROOT_DIRECTORY));
        getLogger().lifecycle("- created Antora root directory: {}", rootDir.getAbsolutePath());

        String antoraYaml = YamlMethods.dumpForAntora(
            buildAntoraYamlMap()
        );

        File antoraYamlFile = writeToFile(rootDir, "antora.yml", antoraYaml);
        if (antoraYamlFile != null) {
            getLogger().lifecycle("- wrote component descriptor to: {}", antoraYamlFile.getAbsolutePath());
        }

        File modulesDir = mkdir(rootDir, "modules");
        getLogger().lifecycle("- created modules directory:     {}", modulesDir.getAbsolutePath());

        getModules().forEach(module -> {
            File moduleDir = mkdir(modulesDir, module);
            ANTORA_FAMILY_DIRECTORIES.forEach(family -> {
                File familyDir = mkdir(new File(moduleDir, family));
                getLogger().lifecycle("- created family directory:      {}", familyDir.getAbsolutePath());
                if (family.equals("pages")) {
                    File indexAdoc = writeToFile(
                        new File(familyDir, "index.adoc"),
                        String.format("= %s module\n\nContent please.\n", module)
                    );
                    if (indexAdoc != null) {
                        getLogger().lifecycle("- wrote dummy page to:           {}", indexAdoc.getAbsolutePath());
                    }
                }
            });
            File navAdoc = writeToFile(moduleDir, "nav.adoc", "* xref:index.adoc[]\n");
            if (navAdoc != null) {
                getLogger().lifecycle("- wrote dummy nav to:            {}", navAdoc.getAbsolutePath());
            }
        });
    }

    private File mkdir(File parent, String file) {
        return mkdir(new File(parent, file));
    }

    private File mkdir(Object dir) {
        return getProject().mkdir(dir);
    }

    private File writeToFile(File parent, String file, String result) {
        return writeToFile(new File(parent, file), result);
    }

    private File writeToFile(File target, String result) {
        if (target.isFile()) {
            getLogger().lifecycle("! skipped overwriting file:      {}", target.getAbsolutePath());
            return null;
        }
        return FileWriterMethods.writeToFile(target, result);
    }

    @SuppressWarnings("UnstableApiUsage")
    private YamlMap buildAntoraYamlMap() {
        String componentName = getComponentName().getOrElse(getProject().getName() + "-docs");
        List<String> modules = getModules();
        return YamlMap.start()
            .with("name", componentName)
            .with("title", getTitle().orElse(capitalize(componentName)).get())
            .with("version", getVersion().getOrElse("master"))
            .withIfValue("prerelease", getPrerelease().getOrNull())
            .with("start_page", modules.get(0) + ":index.adoc")
            .with("nav", createNavList(modules));
    }

    private Provider<String> capitalize(String componentName) {
        return getProject().provider(() ->
            Arrays.stream(componentName.split("-"))
                .filter((it -> !it.isEmpty()))
                .map(it -> "" + Character.toUpperCase(it.charAt(0)) + it.subSequence(1, it.length()))
                .collect(Collectors.joining(" "))
        );
    }

    private static List<String> createNavList(List<String> modules) {
        return modules.stream()
            .map(it -> "modules/" + it + "/nav.adoc")
            .collect(Collectors.toList());
    }
}
