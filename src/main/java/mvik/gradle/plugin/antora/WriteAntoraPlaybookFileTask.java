package mvik.gradle.plugin.antora;

import mvik.gradle.plugin.misc.FileWriterMethods;
import mvik.gradle.plugin.yaml.YamlMap;
import mvik.gradle.plugin.yaml.YamlMethods;
import org.gradle.api.DefaultTask;
import org.gradle.api.Transformer;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class WriteAntoraPlaybookFileTask extends DefaultTask {

    private static final String DEFAULT_UI_BUNDLE = "https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable";
    private static final String DEFAULT_KROKI_SERVER_URL = "https://kroki.io";

    @Optional @Input public abstract Property<Boolean> getRelativizePaths();

    @Optional @Input public abstract Property<String> getTitle();

    @Optional @Input public abstract Property<String> getStartPage();

    @Optional @Input public abstract Property<String> getRepositoryRoot();

    @Optional @Input public abstract ListProperty<String> getStartPaths();

    @Optional @Input public abstract ListProperty<String> getBranches();

    @Optional @Input public abstract ListProperty<String> getTags();

    @Optional @Input public abstract ListProperty<String> getAsciidocExtensions();

    @Optional @Input public abstract Property<String> getKrokiServerUrl();

    @Optional @Input public abstract Property<Boolean> getKrokiFetchDiagram();

    @Optional @Input public abstract Property<Boolean> getAddConvenienceAsciidocAttributes();

    @Optional @Input public abstract MapProperty<String, Object> getAsciidocAttributes();

    @Optional @Input public abstract Property<String> getUiBundleUrl();

    @Optional @Input public abstract Property<String> getUiSupplementalFiles();

    @Optional @Input public abstract Property<String> getOutputSiteDir();

    @OutputFile
    public abstract RegularFileProperty getPlaybookFile();


    @TaskAction
    void writeFile() {

        File playbookFile = getPlaybookFile().getAsFile().get();

        Path repositoryRootPath = Paths.get(
            getRepositoryRoot()
                .getOrElse(getProject().getRootDir().getAbsolutePath())
        );
        Path projectPath = Paths.get(getProject().getProjectDir().toURI());

        Path supplementalFilesPath = decidePath(
            projectPath,
            getUiSupplementalFiles().map(Paths::get).getOrNull()
        );

        Path outputSitePath = decidePath(
            projectPath,
            Paths.get(getOutputSiteDir().getOrElse(getProject().getLayout().getBuildDirectory().file("antora-site").get().getAsFile().getAbsolutePath()))
        );

        List<String> relativeStartPaths = getStartPaths().get().stream()
            .map(Paths::get)
            .map(p -> relativizePath(repositoryRootPath, p))
            .map(Path::toString)
            .collect(Collectors.toList());

        YamlMap yamlMap = PlaybookFileBuilder.createYamlMap(
            getTitle().getOrElse("Local Site"),
            getStartPage().getOrNull(),
            decidePath(projectPath, repositoryRootPath).toString(),
            relativeStartPaths,
            getBranches().map(branchesDefaultToHead()).get(),
            getTags().getOrNull(),
            resolveAsciidocExtensions(playbookFile),
            getKrokiServerUrl().getOrElse(DEFAULT_KROKI_SERVER_URL),
            getKrokiFetchDiagram().getOrElse(false),
            mergeAsciidocAttributes(),
            getUiBundleUrl().getOrElse(DEFAULT_UI_BUNDLE),
            supplementalFilesPath == null ? null : supplementalFilesPath.toString(),
            outputSitePath.toString()
        );

        File file = FileWriterMethods.writeToFile(
            playbookFile,
            YamlMethods.dumpForAntora(yamlMap)
        );

        getLogger().lifecycle("Antora playbook file has been written to: " + file.getAbsolutePath());
    }

    private Path decidePath(Path sourcePath, Path targetPath) {
        if (getRelativizePaths().getOrElse(false)) {
            return relativizePath(sourcePath, targetPath);
        } else {
            return targetPath;
        }
    }

    private static Path relativizePath(Path sourcePath, Path targetPath) {
        if (!(sourcePath.isAbsolute() && targetPath != null && targetPath.isAbsolute())) {
            return targetPath;
        }
        if (sourcePath.toFile().isFile()) { // TODO 2021-04-02: fix, this only works when the file already exists
            return sourcePath.getParent().relativize(targetPath);
        }
        return sourcePath.relativize(targetPath);
    }

    private List<String> resolveAsciidocExtensions(File playbookFile) {

        return getAsciidocExtensions().getOrElse(Collections.emptyList()).stream()
            .map(extension -> {
                Path p = Paths.get(extension);
                if (p.toFile().isFile()) {
                    Path result = decidePath(playbookFile.toPath().getParent(), p);
                    return result.isAbsolute() ? result.toString() : "./" + result;
                }
                return extension;
            })
            .collect(Collectors.toList());
    }

    private Map<String, Object> mergeAsciidocAttributes() {
        Map<String, Object> merged = new HashMap<>();

        if (getAddConvenienceAsciidocAttributes().getOrElse(true)) {
            merged.put("experimental", "");
            merged.put("page-pagination", "");
        }
        if (getAsciidocAttributes().isPresent()) {
            merged.putAll(getAsciidocAttributes().get());
        }
        return merged;
    }

    private Transformer<List<String>, List<String>> branchesDefaultToHead() {
        return branches -> {
            if (branches.isEmpty()) {
                return Collections.singletonList("HEAD");
            }
            return branches;
        };
    }
}
