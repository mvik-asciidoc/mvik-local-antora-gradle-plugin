package mvik.gradle.plugin.antora;

import org.gradle.api.Action;
import org.gradle.api.file.ProjectLayout;
import org.gradle.api.file.RegularFile;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Nested;

import java.io.File;
import java.util.Map;

@SuppressWarnings({"unused"})
public abstract class LocalAntoraExtension {

    public abstract Property<Boolean> getDownloadNode();

    public abstract Property<String> getNodeVersion();

    public abstract Property<String> getAntoraVersion();

    public abstract Property<String> getAsciidoctorCoreVersion();

    public abstract MapProperty<String, String> getAsciidocExtensions();

    @Nested public abstract Kroki getKroki();

    @Nested public abstract Playbook getPlaybook();

    public abstract RegularFileProperty getPlaybookFile();

    public LocalAntoraExtension(ProjectLayout layout) {
        getAsciidocExtensions().put("asciidoctor-kroki", "^0.16.0");
        getAsciidocExtensions().put("@djencks/asciidoctor-mathjax", "^0.0.8");
        getPlaybookFile().convention(layout.getBuildDirectory().file("antora/playbook.yml"));
    }

    public void downloadNode(boolean download) {
        getDownloadNode().set(download);
    }

    public void antoraVersion(String version) {
        getAntoraVersion().set(version);
    }

    public void asciidoctorCoreVersion(String version) {
        getAsciidoctorCoreVersion().set(version);
    }

    public void asciidocExtensions(Map<String, String> extensions) {
        getAsciidocExtensions().set(extensions);
    }

    public void asciidocExtension(String name, String version) {
        getAsciidocExtensions().put(name, version);
    }

    public void asciidocExtension(Object object) {
        asciidocExtension(object.toString(), "");
    }

    public void kroki(Action<? super Kroki> action) {
        action.execute(getKroki());
    }

    public void playbook(Action<? super Playbook> action) {
        action.execute(getPlaybook());
    }

    public void playbookFile(File playbookFile) {
        getPlaybookFile().set(playbookFile);
    }

    public void playbookFile(RegularFile playbookFile) {
        getPlaybookFile().set(playbookFile);
    }

    public void playbookFile(Provider<RegularFile> playbookFile) {
        getPlaybookFile().set(playbookFile);
    }
}
