package mvik.gradle.plugin.antora;

import mvik.gradle.plugin.yaml.YamlList;
import mvik.gradle.plugin.yaml.YamlMap;

import java.util.List;
import java.util.Map;

final class PlaybookFileBuilder {

    static YamlMap createYamlMap(String title,
                                 String startPage,
                                 String repositoryRoot,
                                 List<String> startPaths,
                                 List<String> branches,
                                 List<String> tags,
                                 List<String> asciidocExtensions,
                                 String krokiServerUrl,
                                 boolean krokiFetchDiagram,
                                 Map<String, Object> asciidocAttributes,
                                 String uiBundleUrl,
                                 String supplementalFiles,
                                 String siteDir) {

        return YamlMap.start()
            .with("site", YamlMap.start()
                .with("title", title)
                .withIfNotNull("start_page", startPage)
            )
            .with("content", YamlMap.start()
                .with("sources", YamlList.of(
                    YamlMap.start()
                        .with("url", repositoryRoot)
                        .with("start_paths", startPaths)
                        .with("branches", branches.size() == 1 ? branches.get(0) : branches)
                        .withIfNotEmpty("tags", tags != null && tags.size() == 1 ? tags.get(0) : tags)
                ))
            )
            .with("asciidoc", YamlMap.start()
                .withIfNotEmpty("extensions", asciidocExtensions)
                .with("attributes", mergeAsciidocAttributes(krokiServerUrl, krokiFetchDiagram, asciidocAttributes))
            )
            .with("runtime", YamlMap.start()
                .with("cache_dir", "./.cache/antora")
                .with("fetch", true)
            )
            .with("ui", YamlMap.start()
                .with("bundle", YamlMap.start()
                    .with("url", uiBundleUrl)
                    .with("snapshot", "true")
                )
                .withIfNotNull("supplemental_files", supplementalFiles)
            )
            .with("output", YamlMap.start()
                .with("dir", siteDir)
            );
    }

    private static YamlMap mergeAsciidocAttributes(String krokiServerUrl, boolean krokiFetchDiagram, Map<String, Object> asciidocAttributes) {
        YamlMap attributes = YamlMap.start();
        if (krokiServerUrl != null && !krokiServerUrl.trim().isEmpty()) {
            attributes.put("kroki-server-url", krokiServerUrl);
        }
        attributes.put("kroki-fetch-diagram", krokiFetchDiagram);
        if (asciidocAttributes != null) {
            attributes.putAll(asciidocAttributes);
        }

        return attributes.withoutNullValues();
    }
}
