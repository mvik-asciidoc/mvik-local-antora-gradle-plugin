let registered = false

function register(registry, {file}) {
  if (!registered) {
    console.log('custom dummy extension registered')
    registered = true
  }
}

module.exports.register = register